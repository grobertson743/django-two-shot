from django.urls import path

from receipts.views import (
    ExpenseCategoryCreateView,
    ExpenseCategoryListView,
    ReceiptCreateView,
    ReceiptListView,
    AccountCreateView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_new"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path("accounts/create/", AccountCreateView.as_view(), name="account_new"),
    path("categories/", ExpenseCategoryListView.as_view(), name="category_list"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="category_new",),
]
